import axios from 'axios';

const instance = axios.create({
    baseURL: "http://localhost:8000/predic/?Age=&Weight=&Height=&Neck=&Wrist="
})

export default instance;

// const predictBodyFat = async (inputData) => {
//   try {
//     const response = await axios.post('http://localhost:8000/predic', inputData);
//     return response.data;
//   } catch (error) {
//     console.error('Error predicting body fat:', error);
//     throw error;
//   }
// };

// ตัวอย่างการใช้งานฟังก์ชัน
// const inputData = {
//   Age: 25,
//   Weight: 70.5,
//   Height: 175.0,
//   Neck: 35.0,
//   Wrist: 17.5
// };

// predictBodyFat(inputData)
//   .then(result => console.log('Prediction result:', result))
//   .catch(error => console.error('Error:', error));
