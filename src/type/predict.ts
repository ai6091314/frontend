export default interface Predict {
    Age: number;
    Weight: number;
    Height: number;
    Neck: number;
    Wrist: number;
}
