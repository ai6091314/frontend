import { defineStore } from 'pinia';
import { ref } from 'vue'
import axios from 'axios';
import type Predict from '@/type/predict';

export const usePredictStore = defineStore('Predict', () => {
  const predicts = ref<Predict[]>([]);

  async function get(Age: any, Weight: any, Height: any, Neck: any, Wrist: any) {
    try {
      const res = await axios.get("http://localhost:8000/predic/", {
        params: { Age, Weight, Height, Neck, Wrist }
      });
      predicts.value = res.data;
    } catch (error) {
      console.error('Error fetching prediction data:', error);
    }
  }

  return { predicts, get };
});
